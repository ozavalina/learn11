package org.example.learn11;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

public class SetOfMethodsTest {
    SetOfMethods setOfMethods;

    @Test
    public void testStringContainsEmail() {
        String actualResult = setOfMethods.checkStringContainsEmail("fddsf@ya.ru");
        String expectedResult = "проверено!";
        Assert.assertEquals(actualResult, expectedResult);
    }

    @Test
    public void testStringContainsEmailNegative() {
        Assert.assertThrows(RuntimeException.class, () -> setOfMethods.checkStringContainsEmail("fdsfd.sg"));
    }

    @BeforeMethod
    public void setUp() {
        setOfMethods = new SetOfMethods();
    }

    @DataProvider(name = "provider")
    public Object[][] dataProvider() {
        return new Object[][]{
                {6, true},
                {1, false}
        };
    }

    @Test(dataProvider = "provider")
    public void testCheckParity(Integer number, boolean expectedResult) {
        boolean actualResult = setOfMethods.checkParity(number);

        Assert.assertEquals(actualResult, expectedResult, String.format("Число %d - %s", number, expectedResult ? "нечетное" : "четное"));
    }

    @Test
    public void testGettingStrings() {
        List<String> remainingStrings = setOfMethods.getStrings(Arrays.asList("арбузище", "123456", "12345", "а", "1234567"));
        int actualResult = remainingStrings.size();
        int expectedResult = 4;

        Assert.assertEquals(actualResult, expectedResult, "Получены не те строки");
    }

    @Test
    public void testEmptyStrings() {
        List<String> remainingStrings = setOfMethods.getStrings(null);
        int actualResult = remainingStrings.size();
        int expectedResult = 0;

        Assert.assertEquals(actualResult, expectedResult, "Есть строки");
    }
}
