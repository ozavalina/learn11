package org.example.learn11;

import java.util.ArrayList;
import java.util.List;

public class SetOfMethods {
    /***
     * Метод проверяет, содержит ли строка str корректный email-адрес
     * Возвращает: email корректный - значение "проверено!", email некорректный - исключение IncorrectEmailException
     */
    public String checkStringContainsEmail(String str) {
        String result;
        if (str.contains("@") && str.contains(".")) {
            result = "проверено!";
        } else {
            throw new RuntimeException("IncorrectEmailException");
        }
        return result;
    }

    /***
     * Метод проверяет четность переданного числа
     * Возвращает: true или false
     */
    public boolean checkParity(Integer number) {
        return number % 2 == 0;
    }

    /***
     * Метод принимает список строк.
     * Возвращает список со строками (из числа переданных), длина которых <=6 ИЛИ которые начинаются с "а"
     */
    public List<String> getStrings(List<String> list) {
        List<String> result = new ArrayList<>();
        if (list != null) {
            for (String str : list) {
                if (str.length() <= 6 || str.charAt(0) == 'а') {
                    result.add(str);
                }
            }
        }
        return result;
    }
}